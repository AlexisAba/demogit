package model;

import javax.persistence.*;

@Entity
@Table(name="produit")
public class Produit {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", nullable=false)
	private int id;
	
	@Column(name="nom", nullable=false)
	private String nom;
	
	@Column(name="prix", nullable=false)
	private int prix;
	
	@Version
	private int version;
	
	public Produit(int id, String nom, int prix) {

		this.id = id;
		this.nom = nom;
		this.prix = prix;
	}
	
	public Produit(String nom, int prix) {

		this.nom = nom;
		this.prix = prix;
	}
	
	public Produit() {
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getPrix() {
		return prix;
	}
	public void setPrix(int prix) {
		this.prix = prix;
	}
	
	@Override
	public String toString() {
		return "produit [id=" + id + ", nom=" + nom + ", prix=" + prix + "]";
	}
	
	
}
