package producer;

import java.util.List;

import javax.annotation.PostConstruct;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import model.Produit;
import service.ProductService;

@Component
public class ProductProducer {
	private Log logger = LogFactory.getLog(ProductProducer.class);
	
	private ProductService productService;
	
	@Autowired
	public ProductProducer(ProductService productService) {
		this.productService = productService;
	}
	
	@PostConstruct
	public void produceData() {
		findProducts();
		addOneProduct();
		findProducts();
	}
	
	private void addOneProduct() {
		logger.info("new product");
		productService.addProduct(new Produit("IPhone", 10));
				
	}
	
	private void findProducts() {
		logger.info("find product");
		List<Produit> allProducts = productService.getAllProducts();
		if(allProducts.isEmpty()) {
			logger.info("no product found");
		} else {
			for(Produit foundProduct : allProducts) {
				logger.info(String.format("product with id %d and name %s found :)",
					foundProduct.getId(), foundProduct.getNom()));
			}
		}
		
	}

}
